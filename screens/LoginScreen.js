import React, { useState } from 'react';
import { View, Text, Button } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginScreen = ({ navigation }) => {
  const handleLogin = async () => {
    // Simule une connexion réussie
    const token = 'un_token_valide';
    await AsyncStorage.setItem('token', token);
    
    // Redirige vers le dashboard après la connexion
    navigation.replace('./screens');
  };

  return (
    <View>
      <Text>Écran de connexion</Text>
      <Button title="Se connecter" onPress={handleLogin} />
    </View>
  );
};

export default LoginScreen;
