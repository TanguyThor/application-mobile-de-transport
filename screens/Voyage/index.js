
import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Voyages from './Voyages';
import Depenses from './Depenses';

const Tab = createMaterialTopTabNavigator();



const Voyage = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 10 }, }} name="Voyages" component={Voyages} />
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 10 }, }}  name="Dépenses" component={Depenses} />
    </Tab.Navigator>
  );
};

export default Voyage;
