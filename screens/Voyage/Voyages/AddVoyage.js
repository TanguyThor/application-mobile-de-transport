import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Modal, FlatList, Text, StyleSheet, TextInput, ScrollView } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import url from '../../../api/url';
import { RadioButton } from 'react-native-paper';
import useVoyageModelState from '../../../Entity/VoyageModel';

const AddVoyage = () => {
  const { VoyageModel, setVoyageModel } = useVoyageModelState();

  const [prix, setPrix] = useState('');
  const [recette, setRecette] = useState('');
  
  const [typeChargement, setTypeChargement] = useState('COLIS');
  // setVoyageModel({ ...VoyageModel, typeTransportMarchandise: 'COLIS' })

  const [cautionRetour, setCautionRetour] = useState('');
  const [delaiRetour, setDelaiRetour] = useState('');
  const [nombreColis, setNombreColis] = useState('');
  const [nomColis, setNomColis] = useState('');
  

  // SELECTION LIEU DEPART
const [lieuDepartList, setLieuDepartList] = useState('');
const [selectedLieuDepart, setSelectedLieuDepart] = useState(null);
const [libelleLieuDepart, setLibelleLieuDepart] = useState(null);
const [modalLieuDepartVisible, setModalLieuDepartVisible] = useState(false);

// SELECTION DESTINATION
const [destinationList, setDestinationList] = useState('');
const [selectedDestination, setSelectedDestination] = useState(null);
const [libelleDestination, setLibelleDestination] = useState(null);
const [modalDestinationVisible, setModalDestinationVisible] = useState(false);

  // SELECTION DU CHAUFFEUR
  const [chauffeurList, setChauffeurList] = useState('');
  const [selectedChauffeur, setSelectedChauffeur] = useState(null);
  const [nomChauffeur, setNomChauffeur] = useState(null);
  const [vehicule, setVehicule] = useState(null);
  const [modalChauffeurVisible, setModalChauffeurVisible] = useState(false);

  const calculRecette = () => {
    const v_tonnag = parseFloat(VoyageModel.tonnage);
    const v_prix = parseFloat(VoyageModel.prixUnitaire);

    if (!isNaN(v_tonnag) && !isNaN(v_prix)) {
      const v_recette = v_tonnag * v_prix;
      setRecette(v_recette.toString());
    } else {
      setRecette('Entrez des valeurs numériques valides');
    }
  };

  const fetchListChauffeur = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');

      if (token) { 
        const response = await axios.get(url+'/api/transport/chauffeur/marchandise/associer', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setChauffeurList(response.data.listObject);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des Voyages :', error);
    }
  }; 
 

const fetchListLieuDepart = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');

    if (token) { 
      const response = await axios.get(url+'/api/transport/ville/list', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setLieuDepartList(response.data.listObject);
      setDestinationList(response.data.listObject);
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des LieuDepart :', error);
  }
}; 

const handleRegister = async () => {
  console.log('Enrégistrement en cours...');
  console.log(VoyageModel);
  try {
    const token = await AsyncStorage.getItem('AccessToken');

    if (token) { 
      const response = await axios.post(url+'/api/transport/voyage/save', 
        VoyageModel, {headers: { Authorization: `Bearer ${token}`, },
      });
    }
  } catch (error) {
    console.error('Erreur lors de la Sauvegarde:', error);
  }
};
 

const renduChauffeur = ({ item }) => (
  <TouchableOpacity
    style={styles.listItem}
    onPress={() => {
      
      console.log(item)
      setSelectedChauffeur(item);
      setVehicule(item.idVehicule);
      
      setVoyageModel({ ...VoyageModel, idVehicule: item.idVehicule })
      setVoyageModel({ ...VoyageModel, chauffeur: item })
      
      console.log("chauffeur")
      console.log(selectedChauffeur)
      console.log("afficher")
      console.log("idVehicule")
      console.log(vehicule)
      console.log("afficher")
      setNomChauffeur(item.nom+' '+item.prenom);
      setModalChauffeurVisible(false);
    }}
  >
    <Text>{item.nom+' '+item.prenom}</Text>
  </TouchableOpacity>
);

const renduLieuDepart = ({ item }) => (
  <TouchableOpacity
    style={styles.listItem}
    onPress={() => {
      setSelectedLieuDepart(item);
      setVoyageModel({ ...VoyageModel, lieuDepart: item })
      setLibelleLieuDepart(item.libelle);
      setModalLieuDepartVisible(false);
    }}
  >
    <Text>{item.libelle}</Text>
  </TouchableOpacity>
); 

const renduDestination = ({ item }) => (
  <TouchableOpacity
    style={styles.listItem}
    onPress={() => {
      setSelectedDestination(item);
      setVoyageModel({ ...VoyageModel, lieuArriver: item })
      setLibelleDestination(item.libelle);
      setModalDestinationVisible(false);
    }}
  >
    <Text>{item.libelle}</Text>
  </TouchableOpacity>
);  


useEffect(() => {
  fetchListChauffeur();
  fetchListLieuDepart();
}, []);

 


  return (
    <ScrollView style={{flex:1, backgroundColor:'#D5F0C160'}}>

      <View style={styles.container}>
      
          <View>
            {/* chauffeur */}
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <Text style={styles.label}>Chauffeur</Text>
              <Text style={{fontSize:20 ,marginBottom:5, marginTop:15}}>{vehicule ? `Véhicule: ${vehicule.immatriculation}` : ''}</Text>
            </View>
            
            <TouchableOpacity
              style={styles.dropdownButton}
              onPress={() => setModalChauffeurVisible(true)}
            >
              <Text>{selectedChauffeur ? `${nomChauffeur}` : 'Sélectionner un chauffeur'}</Text>
            </TouchableOpacity>

            <Modal
              animationType="slide"
              transparent={true}
              visible={modalChauffeurVisible}
            >
              <View style={styles.modalView}>
                <FlatList
                  data={chauffeurList}
                  keyExtractor={(item) => item.idPersonnel.toString()}
                  renderItem={renduChauffeur}
                />
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={() => setModalChauffeurVisible(false)}
                >
                  <Text>Fermer</Text>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>

          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            
            <View style={{width:'48%'}}>
              {/* Lieu depart */}
              <Text style={styles.label}>Point de Départ</Text>
              
              <TouchableOpacity
                style={styles.dropdownButton}
                onPress={() => setModalLieuDepartVisible(true)}
              >
                <Text>{selectedLieuDepart ? `${libelleLieuDepart}` : 'Sélectionnez'}</Text>
              </TouchableOpacity>

              <Modal
                animationType="slide"
                transparent={true}
                visible={modalLieuDepartVisible}
              >
                <View style={styles.modalView}>
                  <FlatList
                    data={lieuDepartList}
                    keyExtractor={(item) => item.idVille.toString()}
                    renderItem={renduLieuDepart}
                  />
                  <TouchableOpacity
                    style={styles.closeButton}
                    onPress={() => setModalLieuDepartVisible(false)}
                  >
                    <Text>Fermer</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>

            <View style={{width:'48%'}}>
              {/* Lieu Destination */}
              <Text style={styles.label}>Destination</Text>
              
              <TouchableOpacity
                style={styles.dropdownButton}
                onPress={() => setModalDestinationVisible(true)}
              >
                <Text>{selectedDestination ? `${libelleDestination}` : 'Sélectionnez'}</Text>
              </TouchableOpacity>

              <Modal
                animationType="slide"
                transparent={true}
                visible={modalDestinationVisible}
              >
                <View style={styles.modalView}>
                  <FlatList
                    data={destinationList}
                    keyExtractor={(item) => item.idVille.toString()}
                    renderItem={renduDestination}
                  />
                  <TouchableOpacity
                    style={styles.closeButton}
                    onPress={() => setModalDestinationVisible(false)}
                  >
                    <Text>Fermer</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>

          </View>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'38%'}}>
              {/* Tonne Total */}
              <Text style={styles.label}>Tonne Total</Text>
              <TextInput
                style={styles.input}
                placeholder="Tonne Total"
                keyboardType="numeric"
                value={VoyageModel.tonnage}
                onChangeText={(text) => {
                  setVoyageModel({ ...VoyageModel, tonnage: text });
                  calculRecette();
                }}
              />
              </View>
              <View style={{width:'58%'}}>
                {/* Prix Unitaire */}
                <Text style={styles.label}>Prix Unitaire (XOF)</Text>
                <TextInput
                  style={styles.input}
                  placeholder="Prix Unitaire"
                  keyboardType="numeric"
                  value={VoyageModel.prixUnitaire}
                  onChangeText={(text) => {
                    setVoyageModel({ ...VoyageModel, prixUnitaire: text });
                    calculRecette();
                  }}
                />
              </View>
              
              {/* afficher la recette en bas de Prix Unitaire*/}
          </View>
          <Text>{recette}</Text>
          {/* Type de Chargement  */}
          <Text style={styles.label}>Type de Chargement </Text>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <RadioButton
                value="COLIS"
                status={VoyageModel.typeTransportMarchandise === 'COLIS' ? 'checked' : 'unchecked'}
                onPress={() => {
                  setVoyageModel({ ...VoyageModel, typeTransportMarchandise: 'COLIS' });
                }}
              />
              <Text>Colis</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <RadioButton
                value="CONTENEUR"
                status={VoyageModel.typeTransportMarchandise === 'CONTENEUR' ? 'checked' : 'unchecked'}
                onPress={() => {
                  setVoyageModel({ ...VoyageModel, typeTransportMarchandise: 'CONTENEUR' });
                }}
              />
              <Text>Conteneur</Text>
            </View>
          </View>
          {VoyageModel.typeTransportMarchandise == 'COLIS' 
          ?
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <View style={{width:'48%'}}>
            {/* Nombre de Colis  */}
            <Text style={styles.label}>Nombre de Colis </Text>
            <TextInput
              style={styles.input}
              placeholder="Nombre de Colis "
              value={VoyageModel.nbreArticle}
              onChangeText={(text) => {
                setVoyageModel({ ...VoyageModel, nbreArticle: text });
              }}
            />
          </View>
          <View style={{width:'48%'}}>
            {/* Nom de Colis */}
            <Text style={styles.label}>Nom de Colis</Text>
            <TextInput
              style={styles.input}
              placeholder="Nom de Colis"
              value={VoyageModel.nomarticle}
              onChangeText={(text) => {
                setVoyageModel({ ...VoyageModel, nomarticle: text });
              }}
            />
          </View>
        </View> 
        : 
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
        <View style={{width:'38%'}}>
          {/* Délai de Retour  */}
          <Text style={styles.label}>Délai de Retour </Text>
          <TextInput
            style={styles.input}
            placeholder="Délai de Retour "
            value={VoyageModel.delaiRoute}
            onChangeText={(text) => {
              setVoyageModel({ ...VoyageModel, delaiRoute: text });
            }}
          />
        </View>
        <View style={{width:'58%'}}>
          {/* Caution de Retour */}
          <Text style={styles.label}>Caution de Retour</Text>
          <TextInput
            style={styles.input}
            placeholder="Caution de Retour"
            value={VoyageModel.causionRoute}
            onChangeText={(text) => {
              setVoyageModel({ ...VoyageModel, causionRoute: text });
            }}
          />
        </View>
      </View>
          }
          {/* Chargeur  */}
          {/* <Text style={styles.label}>Chargeur </Text>
          <TextInput
            style={styles.input}
            placeholder="Chargeur "
          /> */}
          {/* Lieu de Chargement  */}
          {/* <Text style={styles.label}>Lieu de Chargement </Text>
          <TextInput
            style={styles.input}
            placeholder="Lieu de Chargement "
          /> */}
          {/* <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'48%'}}>
              
              <Text style={styles.label}>Date Debut Chargement </Text>
              <TextInput
                style={styles.input}
                placeholder="Date Debut Chargement "
              />
            </View>
            <View style={{width:'48%'}}>
              
              <Text style={styles.label}>Date Fin Chargement  </Text>
              <TextInput
                style={styles.input}
                placeholder="Date Fin Chargement  "
              />
            </View>
            </View> */}

          {/* boutton de validation */}
          <TouchableOpacity style={styles.button} onPress={handleRegister} >
            <Text style={styles.buttonText}>Enrégistrer</Text>
          </TouchableOpacity> 

        </View> 
        
      
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#D5F0C160', // Couleur de fond grise
    padding: 16,
  },
  picker: {
    color: '#000', // Couleur du texte à l'intérieur du Picker
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginBottom: 20,
  },
  label: {
    color: '#3333ff',
    fontSize:20,
    fontWeight:'bold',
    color:'#00001080',
    marginBottom:5,
    marginTop:15
  },
  input: {
    height: 40,
    width: '100%',
    paddingLeft: 8,
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13,
    borderColor: 'black',
    borderWidth: 1,
  },
  button: {
    width: '100%',
    backgroundColor: '#3333ff', // Couleur de fond du bouton
    padding: 10,
    borderRadius: 5,
    marginTop:20
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  dropdownButton: {
    padding: 8,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    height: 40,
    width: '100%',
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13
  },
  modalView: {
    marginTop: 'auto',
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 20,
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  closeButton: {
    marginTop: 10,
    padding: 10,
    backgroundColor: 'lightgray',
    borderRadius: 5,
    alignItems: 'center',
  },

});
export default AddVoyage