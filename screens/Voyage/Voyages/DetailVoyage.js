// ModalComponent.js
import React, { useEffect, useState } from 'react';
import {View, Text, Button, StyleSheet, Image, ScrollView, ImageBackground, Alert } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Switch } from 'react-native-switch';

const DetailVoyage = () => {
  const route = useRoute()
  const [debutChargement, setDebutChargement] = useState(false);
  useEffect(() => {
    route.params.item.dateDebutChargement == null ?  setDebutChargement(false) : setDebutChargement(true);
  }, [debutChargement]);
  
  const [finChargement, setFinChargement] = useState(false);
  useEffect(() => {
    route.params.item.dateFinChargement == null ?  setFinChargement(false) : setFinChargement(true);
  }, [finChargement]);
  

  const [departAller, setDepartAller] = useState(false);
  useEffect(() => {
    route.params.item.demarrerAller == false ?  setDepartAller(false) : setDepartAller(true);
  }, [departAller]);
  

  const [arriverAller, setArriverAller] = useState(false);
  useEffect(() => {
    route.params.item.terminerAller == false ?  setArriverAller(false) : setArriverAller(true);
  }, [arriverAller]);
  

  const [departRetour, setDepartRetour] = useState(false);
  useEffect(() => {
    route.params.item.demarrerRetour == false ?  setDepartRetour(false) : setDepartRetour(true);
  }, [departRetour]);
  

  const [arriverRetour, setArriverRetour] = useState(false);
  useEffect(() => {
    route.params.item.terminerRetour == false ?  setArriverRetour(false) : setArriverRetour(true);
  }, [arriverRetour]);
  

  const [debutDechargement, setDebutDechargement] = useState(false)
  useEffect(() => {
    route.params.item.dateDebutDeChargement == null ?  setDebutDechargement(false) : setDebutDechargement(true);
  }, [debutDechargement]);
  

  const [finDechargement, setFinDechargement] = useState(false)
  useEffect(() => {
    route.params.item.dateFinDeChargement == null ?  setFinDechargement(false) : setFinDechargement(true);
  }, [finDechargement]);
  



  const changeStateDebutChargement = () =>{
    setDebutChargement(!debutChargement)
  }
  const changeStateFinChargement = () =>{
    setFinChargement(!finChargement)
    console.log(finChargement);
    if(!finChargement){
      Alert.alert('Confirmation', 'Voulez-vous démarrer le chargement?',
    [
      {
        text:'Oui',
        onPress:()=>{
          console.log("DEMARRER")
        }
      },
      {
        text:'Non',
        onPress:()=>{
        }
      }
    ])
    }
  }
  const changeStateDepartAller = () =>{
    setDepartAller(!departAller)
  }
  const changeStateArriverAller = () =>{
    setArriverAller(!arriverAller)
  }
  const changeStateDepartRetour = () =>{
    setDepartRetour(!departRetour)
  }
  const changeStateArriverRetour = () =>{
    setArriverRetour(!arriverRetour)
  }
  const changeStateDebutDechargement = () =>{
    setDebutDechargement(!debutDechargement)
    console.log(debutDechargement);
    if(!debutDechargement){
      Alert.alert('Confirmation', 'Voulez-vous démarrer le déchargement?',
    [
      {
        text:'Oui',
        onPress:()=>{
          console.log("DEMARRER")
        }
      },
      {
        text:'Non',
        onPress:()=>{
        }
      }
    ])
    }
  }
  const changeStateFinDechargement = () =>{
    setFinDechargement(!finDechargement)
  }
  return (
    
    <ScrollView>
      {/* entete */}
      <View style={styles.header}>
          <Text style={{color:'#fff', margin:10}}>{route.params.item.identifiant}</Text>
          <Text style={{color:'#fff', margin:10}}>{route.params.item !== null ? route.params.item.lieuDepart.libelle+' - '+route.params.item.lieuArriver.libelle : ' '}</Text>
      </View>
      
      {/* entete */}
      <ImageBackground  style={styles.recette} source={require('../../../assets/detailVoyage.jpg')}>
        <Text style={{ fontWeight: 'bold', color:'#fff' }}>{route.params.item.chauffeur?.nom+" "+route.params.item.chauffeur?.prenom+" - "+route.params.item.idVehicule?.immatriculation} </Text>
        <Text style={styles.recetteContent} >
        <Text style={{ fontWeight: 'bold' }}>Recette: </Text>
        {route.params.item !== null ? (
            route.params.item.typeTransportMarchandise === 'COLIS' ? (
              <Text>{route.params.item.recette +' XOF = '+route.params.item.tonnage +' T * '+route.params.item.prixUnitaire+' XOF'}</Text>
            ) : (
            // Ajoutez ce que vous voulez afficher pour le type différent ici
            <Text>{route.params.item.recette+' XOF'}</Text>
            )
        ) : ('')}
        </Text>
        
        {route.params.item !== null ? (route.params.item.typeTransportMarchandise === 'COLIS' ?(<Text style={{color:'#fff'}}>{route.params.item.nbreArticle+' '+route.params.item.nomarticle}</Text> ):('')) : (' ')} 
        {route.params.item !== null ? (route.params.item.typeTransportMarchandise === 'COLIS' ?(<Text style={{color:'#fff'}}>{'Convoyage de '+route.params.item.typeTransportMarchandise}</Text> ):('')) : (' ')} 
        

      </ImageBackground>

      <View style={styles.cardContainer}>

        <View style={styles.leftCard}>
          <View style={styles.cardTitle}>
            <Image source={require('../../../assets/depenseIcon.png')} style={styles.image} /> 
            <Text style={styles.cardTop}>Dépense</Text>
          </View>
          <Text style={styles.cardTopTitle}>1 450 210</Text>
        </View>

        <View style={styles.rightCard}>
          <View style={styles.cardTitle}>
            <Image source={require('../../../assets/maintenanceIcon.png')} style={styles.image} /> 
            <Text style={styles.cardTop}>Maintenance</Text>
          </View>
          <Text style={styles.cardTopTitle}>1 450 210</Text>
        </View>

      </View>

      
      <View style={styles.modalContent}>
        <Text style={{fontWeight:'bold'}}>Autres Informations</Text>
      </View>

      <View style={{flexDirection:'row', marginVertical:4}}>
        
        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Début Chargement</Text>
          
            {route.params.item.dateDebutChargement !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateDebutChargement} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={debutChargement} onValueChange={changeStateDebutChargement} inActiveText='Arrêter'/>
          } 
        </View>

        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Fin de Chargement</Text>
          
            {route.params.item.dateFinChargement !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateFinChargement} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={finChargement} onValueChange={changeStateFinChargement} inActiveText='Arrêter'/>
          } 
        </View>
      </View>

      <View style={{flexDirection:'row', marginVertical:4}}>
        
        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Départ</Text>
          
            {route.params.item.dateDepartAller !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateDepartAller} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={departAller} onValueChange={changeStateDepartAller} inActiveText='Arrêter'/>
          } 
        </View>

        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Arriver</Text>
          
            {route.params.item.dateArriverAller !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateArriverAller} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={arriverAller} onValueChange={changeStateArriverAller} inActiveText='Arrêter'/>
          } 
        </View>
      </View>

      
      <View style={{flexDirection:'row', marginVertical:4}}>
        
        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Debut Déchargement</Text>
          
            {route.params.item.dateDebutDeChargement !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateDebutDeChargement} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={debutDechargement} onValueChange={changeStateDebutDechargement} inActiveText='Arrêter'/>
          } 
        </View>

        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Fin de Déchargement</Text>
          
            {route.params.item.dateFinDeChargement !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateFinDeChargement} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={finDechargement} onValueChange={changeStateFinDechargement} inActiveText='Arrêter'/>
          } 
        </View>
      </View>

      <View style={{flexDirection:'row', marginVertical:4}}>
        
        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Depart Retour</Text>
          
            {route.params.item.dateDepartRetour !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateDepartRetour} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={departRetour} onValueChange={changeStateDepartRetour} inActiveText='Arrêter'/>
          } 
        </View>

        <View style={styles.infos}>
          <Text style={styles.infosTitle}>Arriver Retour</Text>
          
            {route.params.item.dateArriverRetour !== null
            ? 
            <Text style={styles.infosValue}>{route.params.item.dateArriverRetour} </Text>
            : 
            <Switch circleSize={30} switchBorderRadius={14} switchWidthMultiplier={3} backgroundActive='green' backgroundInactive='red' activeText='Démarrer' value={arriverRetour} onValueChange={changeStateArriverRetour} inActiveText='Arrêter'/>
          } 
        </View>
      </View>

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  header:{
    backgroundColor:"#3333ff",
    flexDirection:'row',
    justifyContent:'space-between',
    borderBottomColor: '#fff',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  infos:{
    marginLeft:'5%', 
    marginRight:'5%', 
    padding:10, 
    backgroundColor:'#fff', 
    width:'40%'
  },
  infosTitle:{
    fontSize:15,
    fontWeight:'bold', 
    backgroundColor:'#3333ff', 
    padding:2, 
    marginBottom:20,
    color:'#fff'
  },
  infosValue:{
    fontSize:14,
    color:'#555560'
  },
  recette:{
    height:200,
    backgroundColor:"#3333ff",
    justifyContent:'center',
    alignItems:'center',
    borderBottomRightRadius:20,
    borderBottomLeftRadius:20,
    opacity:0.9

  },
    recetteContent:{
      fontSize:20,
      fontWeight:'bold',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#452165',
      padding:5,
      opacity:1
    },
    cardContainer:{
      flex:1,
      flexDirection:'row',
      justifyContent:'space-between'
    },
    leftCard:{
      height:80,
      width:150,
      backgroundColor:'#fff',
      top:-40,
      left:20,
      borderRadius:20,
      elevation:1,
    },
    rightCard:{     
      height:80,
      width:150,
      backgroundColor:'#fff',
      top:-40,
      right:20,
      borderRadius:20,
      elevation:1
    },
    cardTitle:{
      flexDirection:'row'
    },
    image:{
        width:30,
        height:30,
        top:2,
        left:2
    },
    modalContent: {
      padding: 20,
    },
    cardTop:{
      left:10,
      top:5,
      fontWeight:'bold',
      fontSize:15,
    },
    cardTopTitle:{
      left:40,
      fontSize:18,
      color:'#555560'
    }
  });

export default DetailVoyage;
