import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator, RefreshControl } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import url from '../../../api/url';
import DetailVoyage from './DetailVoyage';
import AddVoyage from './AddVoyage';
import { Logo } from '../../Logo/Logo';
import { useNavigation } from '@react-navigation/native';

const Voyages = () => {
  const [voyages, setVoyages] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigation = useNavigation();
  const [refreshing, setRefreshing] = useState(false);

  const handleOnPress = (item) => {
    navigation.navigate('DetailVoyage', { item: item })
  };

  const handleAdd = () => {
    navigation.navigate('AddVoyage')
  };
  
  const handleRefresh = () => {
    console.log("freshhhhh")
    setRefreshing(true); // Active le rafraîchissement
     fetchVoyages(); // Chargez les données à nouveau
    // setRefreshing(false);
  };

  const fetchVoyages = async () => {
    try {
      setLoading(true);
      const token = await AsyncStorage.getItem('AccessToken');

      if (token) { 
        const response = await axios.get(url+'/api/transport/voyagemarchandise/listterminer', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setVoyages(response.data.listObject);
        setLoading(false);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
      setLoading(false);
    }
  }; 
  
  useEffect(() => {
    fetchVoyages();
  }, []);

  

  return (
    <View style={styles.container}>
       {loading ? (
        <ActivityIndicator size="large" color="#3333ff" />
      ) : (
          <FlatList
            data={voyages}
            keyExtractor={(item) => item.identifiant.toString()}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
                colors={['#0000ff']} // Couleurs du rafraîchissement (Android)
                tintColor={'#0000ff'} // Couleur du rafraîchissement (iOS)
              /> }
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => handleOnPress(item)}>
            <View style={styles.card}>
                <Logo identifiant={item.identifiant}/>
                <View style={{marginLeft:5}}>
                  <Text style={{fontWeight:'bold'}}>{item.chauffeur?.nom+" "+item.chauffeur?.prenom+" - "+item.idVehicule?.immatriculation}</Text>
                  <Text>{item.lieuDepart?.libelle} - {item.lieuArriver?.libelle}</Text> 
                  <Text>{item.recette} XOF</Text> 
                </View>
            </View> 
            </TouchableOpacity>
        )}
      />
      )}
      <TouchableOpacity onPress={() => handleAdd()} style={styles.addBoutton}>
          <Text style={styles.addBouttonText}>+</Text> 
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor:"#f0f0f0",
    padding:30,
    paddingHorizontal:10,
    flex: 1,
  },
  card: {
    backgroundColor:'#fff',
    borderRadius:8,
    padding:15,
    marginBottom:10,
    flex:1,
    flexDirection:'row',
  },
  addBoutton:{
    position:'absolute',
    zIndex:11,
    right:10,
    bottom:10,
    backgroundColor:'#457854',
    width:50,
    height:50,
    borderRadius:50,
    alignItems:'center',
    justifyContent:'center',
    elevation:8
  },
  addBouttonText:{
    color:'#fff',
    fontSize:30
  }
});

export default Voyages;



