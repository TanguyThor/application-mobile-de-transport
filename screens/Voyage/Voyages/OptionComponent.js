
// OptionComponent.js
import React from 'react';
import { Modal, View, Text, Button, StyleSheet, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

const OptionComponent = ({ isVisible, onClose, itemData }) => {
    // console.log(itemData);
  return (
    <Modal transparent visible={isVisible} animationType="slide">
      <TouchableOpacity style={styles.closeButton} onPress={onClose}>
            <AntDesign name="close" size={24} color="black" />
          </TouchableOpacity>
      <View 
      style={{
        borderRadius:8,
        borderColor:"#333",
        borderWidth:1,
        backgroundColor:"#fff", 
        paddingHorizontal:10, 
        position:"absolute",
        top:100, 
        right:0,
        }}>
        <Text style={{color:"#005", fontWeight:"bold"}}> Options</Text> 
      </View>      
    </Modal>
  );
};
const styles = StyleSheet.create({
    modalContainer: {
      flex: 1,
      backgroundColor: 'rgba(0, 0, 0, 0.5)', // Opacité de moitié sur l'arrière-plan
    },
    closeButton: {
      position: 'absolute',
      top: 10,
      right: 10,
    },
    modalContent: {
       textAlign:'center',
      backgroundColor: 'white',
      width:'95%',
      height:'80%',
      borderRadius: 10, // Bords arrondis
      padding: 20,
      margin: 10, // Marge de 50
    },
  });

export default OptionComponent;
