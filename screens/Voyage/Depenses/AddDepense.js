import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Modal, FlatList, Text, StyleSheet, TextInput, ScrollView } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import url from '../../../api/url';
import { RadioButton } from 'react-native-paper';

const AddDepense = () => {

  // SELECTION DU Voyage
  const [voyageList, setVoyageList] = useState('');
  const [libelle, setLibelle] = useState('');
  const [typeDepense, setTypeDepense] = useState('');
  const [prix, setPrix] = useState('');
  const [quantite, setQuantite] = useState('');
  const [montant, setMontant] = useState('');
  const [selectedVoyage, setSelectedVoyage] = useState(null);
  const [libelleVoyage, setLibelleVoyage] = useState(null);
  const [modalVoyageVisible, setModalVoyageVisible] = useState(false);

  const fetchListVoyage = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');

      if (token) { 
        const response = await axios.get(url+'/api/transport/voyagemarchandise/list', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setVoyageList(response.data.listObject);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  }; 
  
  const calculMontant = () => {
    const v_quantite = parseFloat(quantite);
    const v_prix = parseFloat(prix);

    if (!isNaN(v_quantite) && !isNaN(v_prix)) {
      const v_montant = v_quantite * v_prix;
      setMontant(v_montant.toString());
    } else {
      setMontant('Entrez des valeurs numériques valides');
    }
  };

  const renduVoyage = ({ item }) => (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => {
        setSelectedVoyage(item.idVoyage);
        setLibelleVoyage(item.identifiant+' ('+item.lieuDepart?.libelle+' - '+item.lieuArriver?.libelle+' / '+item.chauffeur?.nom+' '+item.chauffeur?.prenom+')');
        setModalVoyageVisible(false);
      }}
    >
      <Text>{item.identifiant+' ('+item.lieuDepart?.libelle+' - '+item.lieuArriver?.libelle+' / '+item.chauffeur?.nom+' '+item.chauffeur?.prenom+')'}</Text>
    </TouchableOpacity>
  );



const handleRegister = async () => {
  console.log('Enrégistrement en cours...');
  // try {
  //   const token = await AsyncStorage.getItem('AccessToken');

  //   if (token) { 
  //     const response = await axios.post(url+'/api/transport/ville/list', {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     });
  //   }
  // } catch (error) {
  //   console.error('Erreur lors de la récupération des LieuDepart :', error);
  // }
};

useEffect(() => {
  fetchListVoyage();
  console.log("Liste de Voyage Récupérée");
}, []);
 


  return (
    <ScrollView style={{flex:1, backgroundColor:'#D5F0C160'}}>

      <View style={styles.container}>
      
          <View>
            {/* Voyage */}
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <Text style={styles.label}>Voyage</Text>
            </View>
            
            <TouchableOpacity
              style={styles.dropdownButton}
              onPress={() => setModalVoyageVisible(true)}
            >
              <Text>{selectedVoyage ? `${libelleVoyage}` : 'Sélectionner un Voyage'}</Text>
            </TouchableOpacity>

            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVoyageVisible}
            >
              <View style={styles.modalView}>
                <FlatList
                  data={voyageList}
                  keyExtractor={(item) => item.idVoyage.toString()}
                  renderItem={renduVoyage}
                />
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={() => setModalVoyageVisible(false)}
                >
                  <Text>Fermer</Text>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>

          <Text style={styles.label}>Libellé</Text>
          <TextInput
            style={styles.input}
            placeholder="Libellé"
            value={libelle}
            onChangeText={(text) => {
              setLibelle(text);
            }}
          />

          <Text style={styles.label}>Type de Dépense </Text>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <RadioButton
                value="ALLER"
                status={typeDepense === 'ALLER' ? 'checked' : 'unchecked'}
                onPress={() => setTypeDepense('ALLER')}
              />
              <Text>ALLER</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <RadioButton
                value="RETOUR"
                status={typeDepense === 'RETOUR' ? 'checked' : 'unchecked'}
                onPress={() => setTypeDepense('RETOUR')}
              />
              <Text>RETOUR</Text>
            </View>
          </View>

          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'38%'}}>
              {/* Prix Unitaire */}
              <Text style={styles.label}>Prix Unitaire</Text>
              <TextInput
                style={styles.input}
                placeholder="Prix Unitaire"
                keyboardType="numeric"
                value={prix}
                onChangeText={(text) => {
                  setPrix(text);
                  calculMontant();
                }}
              />
              </View>
              <View style={{width:'58%'}}>
                {/* Quantité */}
                <Text style={styles.label}>Quantité</Text>
                <TextInput
                  style={styles.input}
                  placeholder="Quantité"
                  keyboardType="numeric"
                  value={quantite}
                  onChangeText={(text) => {
                    setQuantite(text);
                    calculMontant();
                  }}
                />
              </View>
          </View>
          <Text>{montant}</Text>
          

         
          {/* boutton de validation */}
          <TouchableOpacity style={styles.button} onPress={handleRegister} >
            <Text style={styles.buttonText}>Enrégistrer</Text>
          </TouchableOpacity> 

        </View> 
        
      
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#D5F0C160', // Couleur de fond grise
    padding: 16,
  },
  picker: {
    color: '#000', // Couleur du texte à l'intérieur du Picker
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginBottom: 20,
  },
  label: {
    color: '#3333ff',
    fontSize:20,
    fontWeight:'bold',
    color:'#00001080',
    marginBottom:5,
    marginTop:15
  },
  input: {
    height: 40,
    width: '100%',
    paddingLeft: 8,
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13,
    borderColor: 'black',
    borderWidth: 1,
  },
  button: {
    width: '100%',
    backgroundColor: '#3333ff', // Couleur de fond du bouton
    padding: 10,
    borderRadius: 5,
    marginTop:20
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  dropdownButton: {
    padding: 8,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    height: 40,
    width: '100%',
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13
  },
  modalView: {
    marginTop: 'auto',
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 20,
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  closeButton: {
    marginTop: 10,
    padding: 10,
    backgroundColor: 'lightgray',
    borderRadius: 5,
    alignItems: 'center',
  },

});
export default AddDepense