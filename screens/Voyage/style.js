import { StyleSheet } from "react-native";

const voyageStyle = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
      },
      image: {
        width: 70, // Ajustez la largeur de l'image selon vos besoins
        height: 70, // Ajustez la hauteur de l'image selon vos besoins
        marginRight: 16,
        borderRadius: 10, // Pour un cercle, utilisez la moitié de la largeur/hauteur
      },
      text: {
        fontSize: 16,
        fontWeight:'bold'
      },
});

export default voyageStyle;