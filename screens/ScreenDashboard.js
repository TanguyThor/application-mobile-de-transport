import { View, Text } from 'react-native'
import React from 'react'
import Routes from './routes'

const ScreenDashboard = () => {
  return (
    <View style={{flex:1}}>
      <Routes/>
    </View>
  )
}

export default ScreenDashboard