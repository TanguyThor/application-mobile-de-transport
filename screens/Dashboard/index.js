import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, View, StyleSheet, Text, Image, ScrollView } from 'react-native';
import url from '../../api/url';
import Statistics from './Statistics';

const Dashboard = () => {
  
  const [voyageEnCours, setvoyageEnCours] = useState(0);
  const [recette, setrecette] = useState(0);
  const [depense, setdepense] = useState(0);
  const [resultat, setresultat] = useState(0);
  const [maintenance, setmaintenance] = useState(0);
  const [reparation, setreparation] = useState(0);
  const [global, setglobal] = useState(0);

  const fetchVoyageEnCours = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/statistique/voyageencours', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
         setvoyageEnCours(response.data);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  };

  const fetchRecette = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/statistique/sommerecettes', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
         return response.data;
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  };

  const fetchDepense = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/statistique/sommedepenses', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
         return response.data;
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  };

  const fetchMaintenance = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/statistique/sommemaintenances', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
         return response.data;
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  };

  const fetchReparation = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/statistique/sommereparations', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
         return response.data;
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  };

  useEffect( () => {
    const fetchData = async () => {
      try {
        // Appeler votre première fonction
        const recetteData = await fetchRecette();
        setrecette(recetteData);

        // Appeler votre deuxième fonction
        const depenseData = await fetchDepense();
        setdepense(depenseData);

        // Additionner les valeurs de recette et depense
        const sum = recetteData - depenseData;

        // Mettre à jour l'état avec le résultat total
        setresultat(sum);
        
        // Appeler votre première fonction
        const recetteMaintenance = await fetchMaintenance();
        setmaintenance(recetteMaintenance);

        // Appeler votre deuxième fonction
        const depenseReparation = await fetchReparation();
        setreparation(depenseReparation);

        // Additionner les valeurs de recette et depense
        const diff = sum - recetteMaintenance - depenseReparation;

        // Mettre à jour l'état avec le résultat total
        setglobal(diff);
        
      } catch (error) {
        console.error('Erreur lors de la récupération des données :', error);
      }
    };

    // Appeler la fonction fetchData
    fetchData();
  
    // Appel de la fonction pour récupérer les données
    fetchVoyageEnCours();
    fetchMaintenance();
    fetchReparation();
    setglobal(recette - depense - maintenance - reparation);
    
  }, []); // Le tableau vide [] en tant que deuxième argument signifie que useEffect sera exécuté une seule fois après le montage du composant


  return (
    <ScrollView>
    <View style={{margin:10}}>
      
      <Image source={require('../../assets/voyages.jpg')} style={styles.image} />
      
      <Text style={styles.titre}>Application de Gestion de Transport</Text>
      <Text style={styles.subTitle}>Gestion de rendement transport terrestre</Text>


      <View style={{ marginVertical:10}}>
        <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
          
      <Text style={{fontWeight:'bold'}}>Statistiques Budgétaires</Text>
      {/* <Text style={{color:'#1C23F0'}}>Détail</Text> */}
        </View>
      </View>
      
      <ScrollView horizontal  showsHorizontalScrollIndicator={false} style={{marginVertical:10}}>
        
        <Statistics img={require('../../assets/assurance.png')} title={'Voyages en cours'} subTitle={voyageEnCours.toLocaleString()}/>
        
        <Statistics img={require('../../assets/benefices.png')} title={'Recette'} subTitle={recette.toLocaleString()+' Fcfa'}/>
        
        <Statistics img={require('../../assets/depense.jpg')} title={'Dépenses'} subTitle={depense.toLocaleString()+' Fcfa'}/>
        
        <Statistics img={require('../../assets/maintenance.jpg')} title={'Résultat'} subTitle={resultat.toLocaleString()+' Fcfa'}/>
     
      </ScrollView>

<View style={{ marginVertical:10}}>
  <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
    <Text style={{fontWeight:'bold'}}>Statistiques Techniques</Text>
  </View>
</View>

<ScrollView horizontal showsHorizontalScrollIndicator={false} style={{marginVertical:10}}>

  <Statistics img={require('../../assets/recettes.jpg')} title={'Maintenance'} subTitle={maintenance.toLocaleString()+' Fcfa'}/>
  
  <Statistics img={require('../../assets/visitetech.jpeg')} title={'Réparation'} subTitle={reparation.toLocaleString()+' Fcfa'}/>
  
  <Statistics img={require('../../assets/reparation.png')} title={'Global'} subTitle={global.toLocaleString()+' Fcfa'}/>


</ScrollView>

    </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  card: {
    width: 200,
    height: 150,
    margin: 10,
    backgroundColor: 'lightgray',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  image: {
    width: '100%',
    height: 150,
    marginBottom: 16,
    borderRadius:10
  },
  titre:{
    fontWeight:'bold',
    fontSize:20
  },
  subTitle:{

  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  title:{
    fontWeight:'bold',
    fontSize:20,
  },
  value:{
  },
  item: {
    backgroundColor: '#656D97',
    padding: 5,
    marginVertical: 5,
    width: '45%', // Ajustez la largeur en fonction de vos besoins
    borderRadius: 10,
  },
  itemOne: {
    backgroundColor: '#656D97',
    padding: 5,
    marginVertical: 5,
    width:'100%',
    borderRadius: 10,
  },
});

export default Dashboard;
