import { Image, StyleSheet, Text, View } from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";

export default function Statistics({img, title, subTitle}){
    return(
        <View style={style.container}>
            <Image source={img} style={style.img} />

            <View style={{padding:7}}>
                <Text style={style.title}>{title}</Text>
                <Text style={style.subTitle}>{subTitle}</Text>
            </View>
        </View>
    );
}

const style = StyleSheet.create({
    container:{
        width:200,
        borderWidth:1,
        borderColor:'#C6C4B7',
        borderRadius:10,
        marginRight:15
    },
    img:{
        width:'100%',
        height:110,
        borderTopLeftRadius:10,
        borderTopRightRadius:10
    },
    title:{
        fontWeight:'bold',
        fontSize:20
    },
    subTitle:{
        color:'#70706E',
        fontSize:16
    }
})