export const fetchVoyageEnCours = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');
    if (token) { 
      const response = await axios.get(url+'/api/transport/statistique/voyageencours', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
       setvoyageEnCours(response.data);
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des voyages :', error);
  }
};

export const fetchRecette = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');
    if (token) { 
      const response = await axios.get(url+'/api/transport/statistique/sommerecettes', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
       return response.data;
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des voyages :', error);
  }
};

export const fetchDepense = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');
    if (token) { 
      const response = await axios.get(url+'/api/transport/statistique/sommedepenses', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
       return response.data;
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des voyages :', error);
  }
};

export const fetchMaintenance = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');
    if (token) { 
      const response = await axios.get(url+'/api/transport/statistique/sommemaintenances', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
       return response.data;
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des voyages :', error);
  }
};

export const fetchReparation = async () => {
  try {
    const token = await AsyncStorage.getItem('AccessToken');
    if (token) { 
      const response = await axios.get(url+'/api/transport/statistique/sommereparations', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
       return response.data;
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des voyages :', error);
  }
};