import { Text, View } from "react-native";

function changerCouleur(){
    let letter = "0123456789ABCDEF";
    let color = "#";

    for(let i=0; i<6; i++){
        color+= letter [Math.floor(Math.random()*16)];
    }
    return color;
}
export const Logo = ({identifiant}) =>{
    return(
        <View style={{backgroundColor:'#C08A0C',width:56, height:56, borderRadius:30,textAlign:"center"}}>
            <View style={{backgroundColor:'#fff', marginTop:1, marginLeft:1, width:49, height:49, borderRadius:30,alignContent:"center"}}>
            <View style={{backgroundColor:'#3333ff', marginTop:1, marginLeft:1, width:42, height:42, borderRadius:30,alignContent:"center"}}>
                <Text style={{textAlign:"center", color:"#fff", justifyContent:"center", marginTop:12, fontSize:13}}>{identifiant}</Text>
            </View> 
            </View> 
        </View> 
    );
}