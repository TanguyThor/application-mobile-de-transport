import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import BottomTabs from '../tab'

const Stack =  createNativeStackNavigator();

const Routes = () => {
  return (
      <Stack.Navigator initialRouteName='Dashboard' screenOptions={{headerShown:false}}>
        <Stack.Screen name='BottomTabs' component={BottomTabs}></Stack.Screen>
      </Stack.Navigator>
  )
}

export default Routes