import React, { useEffect, useState } from 'react';
import { View, Image, TouchableOpacity, Text, Button, StyleSheet, TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { user_login } from '../../api/user_api';
import jwt_decode from 'jwt-decode';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


  

const Login = ({ navigation }) => {

  
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loggingIn, setLoggingIn] = useState(false);
  const [isDataTokens, setIsDataTokens] = useState(false);
  const [show, setshow] = useState(false);

  const [usernameError, setUsernameError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const handleUsernameChange = (text) => {
    setUsername(text);
    setUsernameError('');
  };

  const handlePasswordChange = (text) => {
    setPassword(text);
    setPasswordError('');
  };

  const handleGetToken = async () => {
    const dataToken = await AsyncStorage.getItem("AccessToken");
    setIsDataTokens(dataToken)
    if(!dataToken){
      navigation.navigate("Login");
    }else{
      navigation.navigate("ScreenDashboard");
    }
  }


  const handleLogin = () => {
    
    if (!username.trim()) {
      setUsernameError('Le nom d\'utilisateur est requis');
      return;
    } else {
      setUsernameError('');
    }

    if (!password.trim()) {
      setPasswordError('Le mot de passe est requis');
      return;
    } else {
      setPasswordError('');
    }

    setLoggingIn(true);

    user_login({
      username: username,
      password: password
    })
      .then(result => {
        if (result.status === 200) {
          const tokenOne = result.headers.authorization.split(' ')[1]
          AsyncStorage.setItem('AccessToken', tokenOne);
          console.log("Connection Réussi");
          handleGetToken()
        }
      })
      .catch(err => {
        console.log(err);
        Alert.alert("Connection impossible, Vérifiez vos identifiants");
      });
      setTimeout(() => {
        setLoggingIn(false); // Définir l'état de connexion terminée
      }, 2000); 
  };
const tok = AsyncStorage.getItem('AccessToken');
//   const decodeur = ({ toked }) => {
//     useEffect(() => {
//       if (toked) {
//         try {
//           // Utilisez jwt_decode au lieu de jwtDecode
//           const decodedToken = jwt_decode(toked);
  
//           // Accédez aux informations du token décodé
//           const username = decodedToken.user;
//           // Autres informations disponibles dans le token décodé...
//           console.log('Nom d\'utilisateur :', username);
//         } catch (error) {
//           // Gérez les erreurs de décodage ici
//           console.error('Erreur lors du décodage du token :', error);
//         }
//       }
//     }, [tok]);
// return 
//   };
  // decodeur(); 

  return (
    <View style={{flex:1}}>
     
      {isDataTokens ? (
        <View style={styles.container}>
          <TouchableOpacity style={styles.buttons} onPress={handleGetToken} >
            <Text style={styles.buttonText}>Naviguez Vers le Dashboard </Text>
          </TouchableOpacity>
        </View>

      ):(

        <View style={styles.container}>

          <Image source={require('../../assets/auth.png')} style={styles.image} />
          <Text style={styles.title}>Authentification</Text>

          {/* username */}
          <Text style={styles.label}>Nom d'utilisateur</Text>
          <TextInput
            style={styles.input}
            placeholder="Nom d'Utilisateur"
            value={username}
            onChangeText={handleUsernameChange}
          />
          <Text style={styles.errorText}>{usernameError}</Text>

          {/* password */}
          <Text style={styles.label}>Mot de passe</Text>
          <View style={{flex:1, flexDirection:'row' ,justifyContent:'center', width:'100%'}}> 
            {/* <MaterialCommunityIcons name="lock-outline" color={'#3333ff'} size={20}  style={styles.iconDebut2}  /> */}
             
            <TextInput
              style={styles.input}
              placeholder="Mot de Passe"
              secureTextEntry={show}
              value={password}
              onChangeText={handlePasswordChange}
            />
            <TouchableOpacity style={styles.iconPassword}  onPress={handleLogin} >
              <MaterialCommunityIcons name={show === false ? 'eye-outline' : 'eye-off-outline'} color={'#3333ff'} size={20} onPress={()=>{setshow(!show)}} />
            </TouchableOpacity> 
          </View>
          <Text style={styles.errorText}>{passwordError}</Text>

          {/* boutton de validation */}
          <TouchableOpacity style={styles.button} onPress={handleLogin} disabled={loggingIn}>
            <Text style={styles.buttonText}>{loggingIn ? 'Connexion en cours...' : 'Se connecter'}</Text>
          </TouchableOpacity> 

        </View> 
        
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F9F6F6', // Couleur de fond grise
    padding: 16,
  },
  iconDebut2:{
   
  },
  iconPassword:{
    position:'absolute',
    right:10,
    top:10,
  },
  image: {
    width: '100%',
    height: 150,
    marginBottom: 10,
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginBottom: 20,
  },
  label: {
    color: '#3333ff',
  },
  input: {
    height: 40,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingLeft: 8,
    // color: 'white',
    backgroundColor:'white'
  },
  button: {
    width: '100%',
    backgroundColor: '#3333ff', // Couleur de fond du bouton
    padding: 10,
    borderRadius: 5,
  },
  buttons: {
    marginTop:10,
    backgroundColor: '#3333ff', // Couleur de fond du bouton
    padding: 10,
    color: 'red',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  title:{
    fontWeight:'bold',
    fontSize: 20,
    marginTop:10,
    marginBottom:10

  }
});

export default Login;
