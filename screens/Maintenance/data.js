const voyageData = [
    { 
        id: '1', 
        name: 'Visite Technique',
        img: require('../../assets/visitetech.jpeg')
     },
     { 
         id: '2', 
         name: 'Réparation',
         img: require('../../assets/reparation.png')
      },
      { 
          id: '3', 
          name: 'Maintenance',
          img: require('../../assets/maintenance.jpg')
       },
       { 
           id: '4', 
           name: 'Assurance',
           img: require('../../assets/assurance.png')
        },
  ];

export default voyageData;