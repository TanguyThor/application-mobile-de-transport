import React from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Pannes from './Pannes';

const Tab = createMaterialTopTabNavigator();

const Reparations = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 10 }, tabBarLabel: 'Panne' }}  name="Pannes" component={Pannes} />
    </Tab.Navigator>
  );
};


export default Reparations;
