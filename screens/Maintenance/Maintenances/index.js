import React from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Entretiens from './Entretiens';
import Vidanges from './Vidanges';

const Tab = createMaterialTopTabNavigator();

const Maintenances = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 10 }, tabBarLabel: 'Entretien' }}  name="Entretiens" component={Entretiens} />
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 10 }, }}  name="Vidange" component={Vidanges} />
    </Tab.Navigator>
  );
};


export default Maintenances;
