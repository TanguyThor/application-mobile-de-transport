import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator, RefreshControl  } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DetailEntretien from './DetailEntretien';
import url from '../../../../api/url';
import { Logo } from '../../../Logo/Logo';
import { useNavigation } from '@react-navigation/native';

const Entretiens = () => {
  const [entretiens, setEntretiens] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigation = useNavigation();
  const [refreshing, setRefreshing] = useState(false);

  const handleOnPress = (item) => {
    navigation.navigate('DetailEntretien', { item: item })
  };
  const handleAdd = () => {
    navigation.navigate('AddEntretien')
  };
  const handleRefresh = () => {
    console.log("freshhhhh")
    setRefreshing(true); // Active le rafraîchissement
    fetchEntretiens(); // Chargez les données à nouveau
    // setRefreshing(false);
  };
  
  const fetchEntretiens = async () => {
    try {
      setLoading(true);
      const token = await AsyncStorage.getItem('AccessToken');

      if (token) { 
        const response = await axios.get(url+'/api/transport/entretien/marchandise/list', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        //  console.log(response.data.listObject); 
        setEntretiens(response.data.listObject);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des entretiens :', error);
    }finally {
      // Arrêtez le chargement, que la requête réussisse ou échoue
      setLoading(false);
    }
  };
  useEffect(() => {

    fetchEntretiens();
  }, []);

  return (
    <View style={styles.container}>
       {loading ? (
        <ActivityIndicator size="large" color="#3333ff" />
      ) : (
        
      <FlatList
        data={entretiens}
        keyExtractor={(item) => item.idEntretien.idEntretien.toString()}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={handleRefresh}
            colors={['#0000ff']} // Couleurs du rafraîchissement (Android)
            tintColor={'#0000ff'} // Couleur du rafraîchissement (iOS)
          /> }
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => handleOnPress(item)}>
            <View style={styles.card}>
                <Logo identifiant={item.idEntretien.idEntretien}/>
                <View style={{marginLeft:5}}>
                <Text style={{fontWeight:'bold'}}>{item.idEntretien.idVehicule.immatriculation}</Text>
                  <Text>{"Le "+item.idEntretien.dates}</Text>
                  <Text>{item.idEntretien.montant} XOF</Text>
                </View>
            </View> 
            </TouchableOpacity>
        )}
      />
      
      )}
      <TouchableOpacity onPress={() => handleAdd()} style={styles.addBoutton}>
        <Text style={styles.addBouttonText}>+</Text> 
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor:"#f0f0f0",
    padding:30,
    paddingHorizontal:10,
    flex: 1,
  },
  card: {
    backgroundColor:'#fff',
    borderRadius:8,
    padding:15,
    marginBottom:10,
    flex:1,
    flexDirection:'row',
  },
  addBoutton:{
    position:'absolute',
    zIndex:11,
    right:10,
    bottom:10,
    backgroundColor:'#457854',
    width:50,
    height:50,
    borderRadius:50,
    alignItems:'center',
    justifyContent:'center',
    elevation:8
  },
  addBouttonText:{
    color:'#fff',
    fontSize:30
  }
});

export default Entretiens;