// ModalComponent.js
import React, { useEffect, useState } from 'react';
import {View, Text, StyleSheet, Image, ScrollView, FlatList } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Switch } from 'react-native-switch';

const DetailEntretien = () => {
  const route = useRoute()
  console.log(route.params.item)
  
  return (
    <View> 
    <ScrollView>
      {/* entete */}
      <View style={styles.header}>
          <Text style={{color:'#fff', margin:10}}>{"N° "+route.params.item.idEntretien.idEntretien}</Text>
          <View style={{flexDirection:'row'}}>
            <Image source={require('../../../../assets/camion-icon.png')} style={{ width:30, height:30,}} />
            <Text style={{color:'#fff', margin:10}}>{route.params.item.idEntretien?.idVehicule.immatriculation}</Text>
          </View>
      </View>
      
      {/* entete */} 
       <View  style={styles.recette} >
          <Text style={styles.recetteContent} >
            <Text style={{ fontWeight: 'bold' }}> {"Montant Total: "+route.params.item.idEntretien.montant+" XOF"} </Text>
          </Text>
          <Text style={{color:'#fff'}}>{route.params.item.idEntretien.idVehicule.immatriculation}</Text>
          <Text style={{color:'#fff'}}>{"Date: "+route.params.item.idEntretien.dates}</Text>
      </View>
    </ScrollView>

<FlatList
data={route.params.item.idLigneEntretien}
keyExtractor={(item) => item.idLigneEntretien.toString()}
renderItem={({ item }) => (
    <View style={styles.card}>
        <View style={{marginLeft:5}}>
          <Text style={{fontWeight:'bold'}}>{item.libelle}</Text>
          <Text>{"Quantité: "+item.quantite}</Text>
          <Text>{"Prix Unitaire: "+item.prixUnitaire} XOF</Text>
          <Text>{"Total: "+item.montant} XOF</Text>
        </View>
    </View> 
)}
/>
</View> 
  );
};

const styles = StyleSheet.create({
  header:{
    backgroundColor:"#3333ff",
    flexDirection:'row',
    justifyContent:'space-between',
    borderBottomColor: '#fff',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  infos:{
    marginLeft:'5%', 
    marginRight:'5%', 
    padding:10, 
    backgroundColor:'#fff', 
    width:'40%'
  },
  infosTitle:{
    fontSize:15,
    fontWeight:'bold', 
    backgroundColor:'#3333ff', 
    padding:2, 
    marginBottom:20,
    color:'#fff'
  },
  infosValue:{
    fontSize:14,
    color:'#555560'
  },
  recette:{
    height:200,
    backgroundColor:"#3333ff99",
    justifyContent:'center',
    alignItems:'center',
    borderBottomRightRadius:20,
    borderBottomLeftRadius:20,

  },
    recetteContent:{
      fontSize:20,
      fontWeight:'bold',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#452165',
      padding:5,
      opacity:1
    },
    cardContainer:{
      flex:1,
      flexDirection:'row',
      justifyContent:'space-between'
    },
    leftCard:{
      height:80,
      width:150,
      backgroundColor:'#fff',
      top:-40,
      left:20,
      borderRadius:20,
      elevation:1,
    },
    rightCard:{     
      height:80,
      width:150,
      backgroundColor:'#fff',
      top:-40,
      right:20,
      borderRadius:20,
      elevation:1
    },
    cardTitle:{
      flexDirection:'row'
    },
    image:{
        width:30,
        height:30,
        top:2,
        left:2
    },
    modalContent: {
      padding: 20,
    },
    cardTop:{
      left:10,
      top:5,
      fontWeight:'bold',
      fontSize:15,
    },
    cardTopTitle:{
      left:40,
      fontSize:18,
      color:'#555560'
    },
    card: {
      backgroundColor:'#fff',
      borderRadius:8,
      padding:10,
      marginVertical:5,
      flex:1,
      flexDirection:'row',
    },
  });

export default DetailEntretien;
