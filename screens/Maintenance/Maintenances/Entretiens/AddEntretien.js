import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Modal, FlatList, Text, StyleSheet, TextInput, ScrollView } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import url from '../../../../api/url';

const AddEntretien = () => {

  // SELECTION DU CHAUFFEUR
  const [chauffeurList, setChauffeurList] = useState('');
  const [selectedChauffeur, setSelectedChauffeur] = useState(null);
  const [nomChauffeur, setNomChauffeur] = useState(null);
  const [immatriculation, setImmatriculation] = useState(null);
  const [modalChauffeurVisible, setModalChauffeurVisible] = useState(false);

  const fetchListChauffeur = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');

      if (token) { 
        const response = await axios.get(url+'/api/transport/chauffeur/marchandise/associer', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setChauffeurList(response.data.listObject);
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  }; 
  

  const renduChauffeur = ({ item }) => (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => {
        setSelectedChauffeur(item.idPersonnel);
        setNomChauffeur(item.nom+' '+item.prenom);
        setImmatriculation(item.idVehicule.immatriculation);
        setModalChauffeurVisible(false);
      }}
    >
      <Text>{item.nom+' '+item.prenom}</Text>
    </TouchableOpacity>
  );



const handleRegister = async () => {
  console.log('Enrégistrement en cours...');
  // try {
  //   const token = await AsyncStorage.getItem('AccessToken');

  //   if (token) { 
  //     const response = await axios.post(url+'/api/transport/ville/list', {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     });
  //   }
  // } catch (error) {
  //   console.error('Erreur lors de la récupération des LieuDepart :', error);
  // }
};

useEffect(() => {
  fetchListChauffeur();
  console.log("Liste de Chauffeur Récupérée");
}, []);
 


  return (
    <ScrollView style={{flex:1, backgroundColor:'#D5F0C160'}}>

      <View style={styles.container}>
      
          <View>
            {/* chauffeur */}
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <Text style={styles.label}>Chauffeur</Text>
              <Text style={{fontSize:20 ,marginBottom:5, marginTop:15}}>{immatriculation ? `Véhicule: ${immatriculation}` : ''}</Text>
            </View>
            
            <TouchableOpacity
              style={styles.dropdownButton}
              onPress={() => setModalChauffeurVisible(true)}
            >
              <Text>{selectedChauffeur ? `${nomChauffeur}` : 'Sélectionner un chauffeur'}</Text>
            </TouchableOpacity>

            <Modal
              animationType="slide"
              transparent={true}
              visible={modalChauffeurVisible}
            >
              <View style={styles.modalView}>
                <FlatList
                  data={chauffeurList}
                  keyExtractor={(item) => item.idPersonnel.toString()}
                  renderItem={renduChauffeur}
                />
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={() => setModalChauffeurVisible(false)}
                >
                  <Text>Fermer</Text>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>

          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'38%'}}>
              {/* Tonne Total */}
              <Text style={styles.label}>Tonne Total</Text>
              <TextInput
                style={styles.input}
                placeholder="Tonne Total"
                keyboardType="numeric"
              />
              </View>
              <View style={{width:'58%'}}>
                {/* Prix Unitaire */}
                <Text style={styles.label}>Prix Unitaire (XOF)</Text>
                <TextInput
                  style={styles.input}
                  placeholder="Prix Unitaire"
                  keyboardType="numeric"
                />
              </View>
              
              {/* afficher la recette en bas de Prix Unitaire*/}
          </View>
          

         
          {/* boutton de validation */}
          <TouchableOpacity style={styles.button} onPress={handleRegister} >
            <Text style={styles.buttonText}>Enrégistrer</Text>
          </TouchableOpacity> 

        </View> 
        
      
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#D5F0C160', // Couleur de fond grise
    padding: 16,
  },
  picker: {
    color: '#000', // Couleur du texte à l'intérieur du Picker
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginBottom: 20,
  },
  label: {
    color: '#3333ff',
    fontSize:20,
    fontWeight:'bold',
    color:'#00001080',
    marginBottom:5,
    marginTop:15
  },
  input: {
    height: 40,
    width: '100%',
    paddingLeft: 8,
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13,
    borderColor: 'black',
    borderWidth: 1,
  },
  button: {
    width: '100%',
    backgroundColor: '#3333ff', // Couleur de fond du bouton
    padding: 10,
    borderRadius: 5,
    marginTop:20
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  dropdownButton: {
    padding: 8,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    height: 40,
    width: '100%',
    backgroundColor:'white',
    borderRadius:5,
    elevation:5,
    fontSize:13
  },
  modalView: {
    marginTop: 'auto',
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 20,
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  closeButton: {
    marginTop: 10,
    padding: 10,
    backgroundColor: 'lightgray',
    borderRadius: 5,
    alignItems: 'center',
  },

});
export default AddEntretien