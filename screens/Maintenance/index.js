import React from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import styles2 from './style';
import Maintenances from './Maintenances';
import Reparations from './Reparations';
import Assurances from '../Assurance';
import VisiteTechniques from '../VisiteTechnique';

const Tab = createMaterialTopTabNavigator();

const Maintenance = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 8 }, tabBarLabel: 'Maintenances' }}  name="Maintenances" component={Maintenances} />
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 8 }, tabBarLabel: 'Réparations'  }}  name="Réparation" component={Reparations} />
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 8 }, }}  name="Assurances" component={Assurances} />
      <Tab.Screen options={{ tabBarLabelStyle: { fontSize: 8 }, }}  name="Visites Technique" component={VisiteTechniques} />
    </Tab.Navigator>
  );
};

export default Maintenance;
