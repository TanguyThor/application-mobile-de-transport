import { StyleSheet } from "react-native";
import { PADDING } from "../outils/constantes";


const MaintenanceStyles = StyleSheet.create({
    header:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:PADDING.horizontal,
        paddingVertical:PADDING.vertical,
        backgroundColor:'white'
    },
    userImg:{
        width:50,
        height:50,
        borderRadius:50 / 2
    },
    userName:{
        fontSize:16
    },
    voyageContainer:{
        paddingHorizontal:PADDING.horizontal,
        paddingVertical:PADDING.vertical,
        marginTop:15
    },
    voyageCard:{
        flex:1,
        flexDirection:"row",
        backgroundColor:"white",
        elevation:5,
        padding:10,
        paddingHorizontal:PADDING.horizontal,
        paddingVertical:PADDING.vertical,
        marginBottom:20,
        borderRadius:5,
    }



})

export default MaintenanceStyles;