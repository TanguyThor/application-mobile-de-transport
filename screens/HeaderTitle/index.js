import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    MenuProvider,
  } from 'react-native-popup-menu';

const HeaderTitle = () => {
  return (
    <View style={{backgroundColor:'#3333ff',width:44, height:44, borderRadius:30,paddingTop:3, paddingLeft:3, marginRight:20}}>
        <TouchableOpacity >
            <Image
            source={require('../../assets/profile-image.png')}
            style={styles.profileImage}
            />
        </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    profileImage: {
      width: 38,
      height: 38,
      borderRadius: 35,
    },
    name: {
      fontSize: 24,
      fontWeight: 'bold',
      marginBottom: 10,
    },
    info: {
      fontSize: 18,
      marginBottom: 8,
    },
  });

export default HeaderTitle