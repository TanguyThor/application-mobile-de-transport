import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Dashboard from '../Dashboard';
import Maintenance from '../Maintenance';
import Profil from '../Parametre/Profil';
import Voyage from '../Voyage';
import HeaderTitle from '../HeaderTitle';


const Tab = createBottomTabNavigator();

const BottomTabs = () => {
    return (
        <Tab.Navigator
          initialRouteName="Dashboard"
          screenOptions={{
            tabBarActiveTintColor: '#C08A0C',
          }}
        >
          <Tab.Screen
            name="Dashboard"
            component={Dashboard}
            options={{
              headerTitle:()=> <Text style={{fontWeight:'bold', fontSize:18}}>Statistique</Text>,
              headerRight:()=> <HeaderTitle />,
              tabBarLabel: 'Statistique',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen
            name="Voyage"
            component={Voyage}
            options={{
              headerTitle:()=> <Text style={{fontWeight:'bold', fontSize:18}}>Gestion Budgétaire</Text>,
              headerRight:()=> <HeaderTitle />,
              tabBarLabel: 'Gestion Budgétaire',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="truck-fast" color={color} size={size} />
              ),
            //   tabBarBadge: 3,
            }}
          />
          <Tab.Screen
            name="Gestion des Maintenance"
            component={Maintenance} 
            options={{
              headerTitle:()=> <Text style={{fontWeight:'bold', fontSize:18}}>Gestion Technique</Text>,
              headerRight:()=> <HeaderTitle />,
              tabBarLabel: 'Gestion Technique',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="car-key" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen
            name="Profil"
            component={Profil}
            options={{
              headerTitle:()=> <Text style={{fontWeight:'bold', fontSize:18}}>Utilisateur</Text>,
              headerRight:()=> <HeaderTitle />,
              tabBarLabel: 'Utilisateur',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account" color={color} size={size} />
              ),
            }}
          />
          
          
        </Tab.Navigator>
      );
}

export default BottomTabs