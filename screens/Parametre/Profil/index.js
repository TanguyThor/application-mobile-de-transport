import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import { View, Text, Image, StyleSheet, Button } from 'react-native';

const Profil = ({navigation}) => {
  // Exemple de données de profil (à remplacer par vos propres données)
  const handleLogout = async () => {
    // Supprime le token et redirige vers l'écran de connexion
    await AsyncStorage.removeItem('AccessToken');
    navigation.replace('Login');
  };

  return (
    <View style={{flex:1}}>
    <Text style={{marginTop:10, fontWeight:'bold', textAlign:'center'}}>
      Type de Transport: 
      <Text style={{marginTop:10, fontWeight:'300'}}>
        Marchandise
      </Text>
    </Text>
    <View style={styles.container}>

      
      {/* Photo de profil */}
      <Image
        source={require('../../../assets/profile-image.png')}
        style={styles.profileImage}
      />

      {/* Informations du profil */}
      <View >
        <Text style={styles.name}>
        FAGNON Hyppolite
        </Text>
        <Text style={styles.info}>67 64 08 16</Text>
        <Text style={styles.info}>fagnonhyppolite@gmail.com</Text>
        <Text style={{marginTop:10, fontWeight:'bold', textAlign:'center'}}>
        N° Matricule: 
        <Text style={{marginTop:10, fontWeight:'300'}}>
          BF 6491 RB - BX 6301 RB
        </Text>
        </Text>
        <Button title="Se déconnecter" onPress={handleLogout} />
      </View>
    
      {/* Ajoutez d'autres informations du profil ici */}
    </View>

    </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginBottom: 20,
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  info: {
    fontSize: 18,
    marginBottom: 8,
  },
});

export default Profil;
