import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import url from '../api/url';

export const ChauffeurList = async () => {
    try {
      const token = await AsyncStorage.getItem('AccessToken');
      if (token) { 
        const response = await axios.get(url+'/api/transport/chauffeur/marchandise/associer', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        console.log("Liste de Chauffeurs retournée");
        return response.data.listObject;
      }
    } catch (error) {
      console.error('Erreur lors de la récupération des voyages :', error);
    }
  }; 