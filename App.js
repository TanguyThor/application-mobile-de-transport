import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Login from './screens/Login';
import ScreenDashboard from './screens/ScreenDashboard';
import Dashboard from './screens/Dashboard';
import Ecran from './screens/Login/ecran';
import { Text } from 'react-native';
import DetailVoyage from './screens/Voyage/Voyages/DetailVoyage';
import DetailDepense from './screens/Voyage/Depenses/DetailDepense';
import DetailEntretien from './screens/Maintenance/Maintenances/Entretiens/DetailEntretien';
import DetailVidange from './screens/Maintenance/Maintenances/Vidanges/DetailVidange';
import DetailPanne from './screens/Maintenance/Reparations/Pannes/DetailPanne';
import DetailAssurance from './screens/Assurance/DetailAssurance';
import DetailVisiteTechnique from './screens/VisiteTechnique/DetailVisiteTechnique';
import AddVoyage from './screens/Voyage/Voyages/AddVoyage';
import AddDepense from './screens/Voyage/Depenses/AddDepense';
import AddEntretien from './screens/Maintenance/Maintenances/Entretiens/AddEntretien';
import AddVidange from './screens/Maintenance/Maintenances/Vidanges/AddVidange';
import AddPanne from './screens/Maintenance/Reparations/Pannes/AddPanne';
import AddVisiteTechnique from './screens/VisiteTechnique/AddVisiteTechnique';
import AddAssurance from './screens/Assurance/AddAssurance';

const Stack = createStackNavigator();

const App = () => {
  const [initialRoute, setInitialRoute] = useState('Login');

  useEffect(() => {
    // Vérifie si le token est valide
    const checkToken = async () => {
      try {
        const token = await AsyncStorage.getItem('AccessToken');
        if (token) {
          // Si le token est valide, redirige vers le dashboard
          setInitialRoute('ScreenDashboard');
        }
      } catch (error) {
        console.error('Erreur lors de la vérification du token :', error);
      }
    };

    checkToken();
  }, []); // Exécuté une seule fois au montage

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={Dashboard}>
        <Stack.Screen name="Login" options={{title:" "}} component={Login}/>
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="DetailVoyage" options={{title:"Détail Voyage"}} component={DetailVoyage} />
        <Stack.Screen name="DetailDepense" options={{title:"Détail Depense"}} component={DetailDepense} />
        <Stack.Screen name="DetailEntretien" options={{title:"Détail Entretien"}} component={DetailEntretien} />
        <Stack.Screen name="DetailVidange" options={{title:"Détail Vidange"}} component={DetailVidange} />
        <Stack.Screen name="DetailPanne" options={{title:"Détail Panne"}} component={DetailPanne} />
        <Stack.Screen name="DetailVisiteTechnique" options={{title:"Détail Visite Technique"}} component={DetailVisiteTechnique} />
        <Stack.Screen name="DetailAssurance" options={{title:"Détail Assurance"}} component={DetailAssurance} />
        <Stack.Screen options={{ headerShown: false }} name="ScreenDashboard" component={ScreenDashboard}/>
        <Stack.Screen name="AddVoyage" options={{ title:"Ajouter Voyage" }} component={AddVoyage}/>
        <Stack.Screen name="AddDepense" options={{ title:"Ajouter Dépense" }} component={AddDepense}/>
        <Stack.Screen name="AddEntretien" options={{ title:"Ajouter Entretien" }} component={AddEntretien}/>
        <Stack.Screen name="AddVidange" options={{ title:"Ajouter Vidange" }} component={AddVidange}/>
        <Stack.Screen name="AddPanne" options={{ title:"Ajouter Panne" }} component={AddPanne}/>
        <Stack.Screen name="AddVisiteTechnique" options={{ title:"Ajouter Visite Technique" }} component={AddVisiteTechnique}/>
        <Stack.Screen name="AddAssurance" options={{ title:"Ajouter Assurance" }} component={AddAssurance}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
