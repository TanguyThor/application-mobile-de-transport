// VoyageModelState.js

import { useState } from 'react';

const useVoyageModelState = () => {
  const [VoyageModel, setVoyageModel] = useState({
    idVoyage: 0,
        valeur: 0,
        identifiant: '',
        recette: 0,
        revenu: 0,
        tonnage: 0,
        nbreArticle: 0,
        prixUnitaire: 0,
        chargeur: '',
        typeTransportMarchandise: '',
        delaiRoute: 0,
        causionRoute: 0,
        contenantAller: '',
        contenantRetour: '',
        dates: '',
        nomarticle: '',
        dateDebutChargement: '',
        dateFinChargement: '',
        dateDebutDeChargement: '',
        dateFinDeChargement: '',
        dateDepartAller: '',
        dateArriverAller: '',
        dateDepartRetour: '',
        dateArriverRetour: '',
        demarrerAller: false,
        terminerAller: false,
        demarrerRetour: false,
        terminerRetour: false,
        typePosition: '',
        idVehicule: '',
        lieuDepart: '',
        lieuArriver: '',
        chauffeur: '',
        idExercice: ''

  });

  const initializeVoyageModel = () => {
    setVoyageModel({
        idVoyage: 0,
        valeur: 0,
        identifiant: '',
        recette: 0,
        revenu: 0,
        tonnage: 0,
        nbreArticle: 0,
        prixUnitaire: 0,
        chargeur: '',
        typeTransportMarchandise: '',
        delaiRoute: 0,
        causionRoute: 0,
        contenantAller: '',
        contenantRetour: '',
        dates: '',
        nomarticle: '',
        dateDebutChargement: '',
        dateFinChargement: '',
        dateDebutDeChargement: '',
        dateFinDeChargement: '',
        dateDepartAller: '',
        dateArriverAller: '',
        dateDepartRetour: '',
        dateArriverRetour: '',
        demarrerAller: false,
        terminerAller: false,
        demarrerRetour: false,
        terminerRetour: false,
        typePosition: '',
        idVehicule: '',
        lieuDepart: '',
        lieuArriver: '',
        chauffeur: '',
        idExercice: '',
        contenantAller: TypeContenant.CHARGER,
        contenantRetour: TypeContenant.VIDE
    });
  };

  return { VoyageModel: VoyageModel || {}, setVoyageModel, initializeVoyageModel };

};

export default useVoyageModelState;
