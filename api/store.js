// store.js

// import { createStore, combineReducers, applyMiddleware } from 'redux';
// import authReducer from './auth';
// import { Provider } from 'react-redux';
// import thunk from 'redux-thunk'; // Vous pouvez utiliser d'autres middlewares si nécessaire

// const rootReducer = combineReducers({
//   auth: authReducer,
//   // Ajoutez d'autres réducteurs si nécessaire
// });


// const store = createStore(rootReducer, applyMiddleware(thunk));

// export default store;


// store.js

import { createStore, combineReducers } from 'redux';
import authReducer from './auth';

const rootReducer = combineReducers({
  auth: authReducer,
  // Ajoutez d'autres réducteurs si nécessaire
});

const store = createStore(rootReducer);

export default store;




