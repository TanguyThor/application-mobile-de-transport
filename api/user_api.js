import { Alert } from "react-native";
import ApiManager from "./apiManager";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const user_login = async data=>{
  try {
    const result = await ApiManager('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    });
    return result;
  
  } catch (error) {
    console.log(error);
    Alert.alert("Connection au serveur impossible")
    return error
  }
      
};


    export const voyagelist = async  data=>{
      try {
        const result = await ApiManager('/api/transport/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
          },
          data: data,
        });
      
        return result;
      
      } catch (error) {
        console.log("erreur de gestion");
        Alert.alert('Erreur', error);
      }
      
    };