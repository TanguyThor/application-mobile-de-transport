import axios from 'axios';
import urlLogin from './urlLogin';


const ApiManager = axios.create({
    baseURL:urlLogin,
    responseType:'json',
    withCredentials:true,
});

export default ApiManager;